/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciologica;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Cesar Dzib
 */
public class Solucion {
    /**
     * @param args the command line arguments
     */
    public Solucion() {
        // TODO code application logic here
    }
    public void resuelve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Ingresa el tamaño de N x M");
        int casos = input.nextInt();
        ArrayList<String> resultados = new ArrayList();
        for (int i = 0; i < casos; i++) {
            System.out.println("Ingresa Fila "+i);
            int rows = input.nextInt();
            System.out.println("Ingresa columna "+i);            
            int columns = input.nextInt();

            if (rows == columns && rows > 1) {
                if (rows % 2== 0) 
                    resultados.add("L");                    
                else resultados.add("R");	
            }
            else if (rows > columns && columns > 1) {
                if (columns % 2 == 0) 
                    resultados.add("U");
                else 
                    resultados.add("D");
            }
            else if (columns > rows) {
                if (rows % 2 == 0) 
                    resultados.add("L");
                else 
                    resultados.add("R");
            }
            else if (columns == 1) {
                if (rows == 1) 
                    resultados.add("R");
                else 
                    resultados.add("D");
            }
        }
        System.out.println("Restultados");
        for (int i = 0; i < resultados.size(); i++) {
            System.out.println(resultados.get(i));
        }
    }
}
