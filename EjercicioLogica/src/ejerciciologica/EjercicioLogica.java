/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciologica;

import ejerciciologica.Solucion;
/**
 *
 * @author Cesar Dzib
 */
public class EjercicioLogica {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here  
        Solucion solucion = new Solucion();
        //(0,0) -> (0,1) -> (0,2) -> 
        //(1,2) -> (2,2) -> (2,1) -> 
        //(2,0) -> (1,0) -> (1,1)
        //1 1
        //2 2
        //3 1
        //3 3
        solucion.resuelve();
    }
}
